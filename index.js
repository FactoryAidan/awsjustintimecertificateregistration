/*
This node.js Lambda function code creates and attaches an IoT policy to the just-in-time registered certificate.
It also activates the certificate.
The Lambda function is attached as a rule engine action to the registration topic:
	$aws/events/certificates/registered/<caCertificateID>
*/
/*
If your Certificate is encoded to use the Thing.name as the value for the Cert's 'Common Name',
That will affect the Policy and the Assignment of the Thing's Principal.
You can make this script also assign the Certificate as the Thing's Principal.
That will require the Policy to be updated as well because the 'client' will change from the 'CertId' to the thing.
*/

process.env.AWS_REGION					=	'us-west-1';
process.env.CERTIFICATE_MENTIONS_THING	=	true;

const CH	=	require('CertificateHelper.js');
const AWS	=	require('aws-sdk');
const IOT	=	new AWS.Iot({'region':process.env.AWS_REGION,apiVersion:'2015-05-28'});


/*
*	@description	Creates a Policy document String allowing
*					connect, publish, subscribe, receive
*	@param	region			{String}
*	@param	account_id		{String}
*	@param	topic_name		{String}
*	@param	thing_name		{String}
*	@return policy_document	{String}
*/
function createPolicyDocument(params){
	const policy_document	=	{
		Version		:	'2012-10-17',
		Statement	:	[
			{
				Effect	:	'Allow',
				Action	:	[
					'iot:Connect',
				],
			//	"Resource": `arn:aws:iot:${process.env.AWS_REGION}:${account_id}:client/${certificate_id}`
			//	"Resource": `arn:aws:iot:${process.env.AWS_REGION}:${params.account_id}:client/${params.thing_name}`,
				"Resource": `arn:aws:iot:${process.env.AWS_REGION}:${params.account_id}:client/*`,
			},
			{
				Effect	: 'Allow',
				Action	:	[
					'iot:Publish',
					'iot:Receive',
				],
				Resource	:	`arn:aws:iot:${process.env.AWS_REGION}:${params.account_id}:topic/${params.topic_name}/*`,
			},
			{
				Effect	:	 'Allow',
				Action	:	[
					'iot:Subscribe',
				],
				'Resource'	:	`arn:aws:iot:${process.env.AWS_REGION}:${params.account_id}:topicfilter/${params.topic_name}/#`,
			}
		]
	};

	return JSON.stringify(policy_document);
}

function handlerSuccess(response){
	console.log(response);
	return response;
}

function handlerError(error){
	if( !error.code || error.code !== 'ResourceAlreadyExistsException' ){
		console.log(error);
		flag_encountered_error	=	true;	//	inherited scope
		callback(error,null);				//	inherited scope
	}
}

exports.handler	=	async function(event, context, callback) {

	//	Don't touch these
	let flag_encountered_error	=	false;
	const account_id			=	event.awsAccountId.toString().trim();
	
	//	------------------
	//	Derive Certificate Information
	//	------------------
	const certificate			=	{
		id			:	event.certificateId.toString().trim(),
	};
	certificate.arn	=	`arn:aws:iot:${process.env.AWS_REGION}:${account_id}:cert/${certificate.id}`;
	
	if( process.env.CERTIFICATE_MENTIONS_THING ){
		await IOT.describeCertificate({certificateId:certificate.id}).promise().then(response=>{
			certificate.pem			=	response.certificateDescription.certificatePem,
			certificate.properties	=	new CH(certificate.pem).getProperties();
			console.log('CERTIFICATE:',certificate);
		}).catch(handlerError);
		if(flag_encountered_error)	return;	//	Callback has been invoked. Stop executing & save resources.
	}

	//	Customize These
	const topic_name	=	`foo/bar/${certificate.id}`;
	const policy		=	{
		name		:	`Policy_${certificate.id}`,
		document	:	createPolicyDocument({
			region		:	parameters.region,
			account_id	:	account_id,
			topic_name	:	topic_name,
		}),
	};

	//	-------------
	//	Create Policy
	//	-------------
	await IOT.createPolicy({
		policyDocument	:	policy.document,
		policyName		:	policy.name,
	}).promise().then(handlerSuccess).catch(handlerError);
	if(flag_encountered_error)	return;	//	Callback has been invoked. Stop executing & save resources.


	//	----------------------------
	//	Attach Policy to Certificate
	//	AKA. Policy's Principal becomes the Certificate
	//	----------------------------
	await IOT.attachPrincipalPolicy({
		policyName	:	policy.name,
		principal	:	certificate.arn,
	}).promise().then(handlerSuccess).catch(handlerError);
	if(flag_encountered_error)	return;	//	Callback has been invoked. Stop executing & save resources.


	//	---------------------------
	//	Attach Thing to Certificate
	//	AKA. Thing's Principal becomes the Certificate
	//	---------------------------
	if( process.env.CERTIFICATE_MENTIONS_THING ){
		await IOT.attachThingPrincipal({
			thingName	:	certificate.properties.common_name,	//	Used as Thing name in this application.
			principal	:	certificate.arn,
		}).promise().then(handlerSuccess).catch(handlerError);
		if(flag_encountered_error)	return;	//	Callback has been invoked. Stop executing & save resources.
	}

	//	--------------------
	//	Activate Certificate
	//	Certificate Revocation List verification happens here if you'd like
	//	Set newStatus to 'ACTIVE|REVOKED' accordingly
	//	--------------------
	await IOT.updateCertificate({
		certificateId: certificate.id,
		newStatus: 'ACTIVE'
	}).promise().then(response=>{
		console.log(response);
		callback(null, 'Success! Created Certificate, Create Policy, Attached Policy, Activated Certificate: ' + certificate.id);
	}).catch(handlerError);

};//exports.handler
